#!/bin/bash

to_activate='CONFIG_CONFIGFS_FS CONFIG_NVME_CORE CONFIG_BLK_DEV_NVME CONFIG_NVME_TARGET CONFIG_RTC_NVMEM CONFIG_NVMEM CONFIG_KGDB'

for i in $to_activate;do
    sed -i -r -e '/^'"${i}"'=.*$/d' $1
    sed -i -r -e '$a '"${i}"'=y' $1
    sed -i -r -e '/# '"${i}"' is not set/d' $1
done
