#!/bin/bash


# launch target
qemu-system-x86_64 \
    -kernel ./bzImage \
    -m 512M \
    -drive file=./fedora_rootfs.img,if=ide \
    -drive file=./nvme_drive.img,if=none,id=nvme1 \
    -device nvme,drive=nvme1,serial=nvme-1 \
    -net nic,model=virtio \
    -net tap,script=no,downscript=no,vhost=on \
    -net bridge,br=virbr0,helper=/usr/libexec/qemu-bridge-helper \
    -append "root=/dev/sda rw audit=0" &

#launch host
qemu-system-x86_64 \
    -kernel ./bzImage \
    -m 512M \
    -drive file=./fedora_rootfs_host.img,if=ide \
    -net nic,model=virtio \
    -net tap,script=no,downscript=no,vhost=on,helper=/usr/libexec/qemu-bridge-helper \
    -net bridge,br=virbr0 \
    -append "root=/dev/sda rw audit=0" &
